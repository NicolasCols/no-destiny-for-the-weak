﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PrincipalMenu : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject _buttonContinue;
    void Start()
    {
        if (PlayerPrefs.GetString("StartGame")== "Yes")
        {
            _buttonContinue.SetActive(true);
        }
        else
        {
            _buttonContinue.SetActive(false);
        }
    }
    public void NewGame()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetString("StartGame","Yes");
        SceneManager.LoadScene("MundoAbierto");
    }

    public void ContinueGame()
    {

        SceneManager.LoadScene("MundoAbierto");
        
    }

    public void Options()
    {
        
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
}
