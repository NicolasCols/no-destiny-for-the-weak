﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactor : MonoBehaviour
{
    [SerializeField] private GameObject _panelInteract;
    [SerializeField] private Text _text;
    private AudioSource _audio;
    
    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall" || other.tag == "Interaction"||other.tag == "InventoryObject"||other.CompareTag("SaveGame"))
        {
            _panelInteract.SetActive(true);
            if (other.CompareTag("InventoryObject"))
            {
                _text.text = "grab";
            }

            if (other.CompareTag("Interaction"))
            {
                _text.text = "interact";
            }

            if (other.CompareTag("SaveGame"))
            {
                _text.text = "save game";
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _panelInteract.SetActive(false);
            if (other.CompareTag("InventoryObject"))
            {
                _audio.Play();
            }
        }
       /* if (other.tag == "Wall" && Input.GetKeyDown(KeyCode.E))
        { 
           var wells = GameObject.FindGameObjectsWithTag("Wall"); 
           for (int i = 0; i < wells.Length; i++) 
           {
               var iwall = wells[i].GetComponent<IWall>(); 
               iwall.DesableWall(); 
           }
        }*/
    }
    
    private void OnTriggerExit(Collider other)
    {
        _panelInteract.SetActive(false);
    }
}
