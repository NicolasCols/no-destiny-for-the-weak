﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WorldController : MonoBehaviour
{

    public static WorldController singlenton;
    [SerializeField] private Nuvia _nuvia;
    [SerializeField] private GameObject _fireEnemy;
    [SerializeField] private GameObject _Boss;
    

    void Awake()
    {
      //  DontDestroyOnLoad(gameObject);
        if (singlenton != null)
        {
            Destroy(gameObject);
            return;
        }

        singlenton = this;

    }

    void Start()
    {
        if (PlayerPrefs.GetString("NuviaFight","")=="Yes")
        {
            NuviaFight(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void FireEnemy()
    {
        _fireEnemy.SetActive(true);
    }
    public void Boss()
    {
        //
    }
    public void NuviaFight(bool win)
    {
        _nuvia.Friend(win);
        
    }
}
