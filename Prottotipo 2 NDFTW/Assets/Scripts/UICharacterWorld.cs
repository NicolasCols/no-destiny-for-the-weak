﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterWorld : MonoBehaviour
{
    //Edit members
    [Header("Panel Character")]
    [SerializeField] private RectTransform _lifeRef;
    [SerializeField] private RectTransform _life;
    [SerializeField] private Text _lifeText;
    [SerializeField] private RectTransform _energyMax;
    [SerializeField] private RectTransform _energy;
    [SerializeField] private RectTransform _expMax;
    [SerializeField] private Text _expText;
    [SerializeField] private RectTransform _exp;
    [SerializeField] private Text _level;
    [SerializeField] private Text _name;
    [SerializeField] private Image _image;

    [Header("Panel Mission")]
    [SerializeField] private Text _nameMissionText;
    [SerializeField] private Text _missionText;
    [SerializeField] private string[] _nameMission;
    [SerializeField] private string[] _mission;
    
    [Header("Panel Indications")]
    [SerializeField] private GameObject indications;
    
    //Private members
    private float _widthLifeMax;
    private float _widthEnergyMax;
    private float _widthExpMax;
    
    private WaitForSeconds _twoMin  = new WaitForSeconds(60);

    void Start()
    {
        if (PlayerPrefs.GetInt("NewGame", 1) == 1)
        {
            StartCoroutine(Indications());
        }
        Missions();
    }

    void Missions()
    {
        int i= 0;
        if (PlayerPrefs.GetString("NuviaFight","")=="Yes")
        {
            i = 1;
        }

        if (PlayerPrefs.GetString("Arena1","")=="Yes")
        {
            i = 2;
        }
        _nameMissionText.text = _nameMission[i];
        _missionText.text = _mission[i];
    }
    IEnumerator Indications()
    {
        indications.SetActive(true);
        yield return _twoMin;
        indications.SetActive(false);

    }
    public void UpdateLife(float life, float lifeMax)
    {
        if (_widthLifeMax==0)
        {
            _widthLifeMax = _lifeRef.rect.width;
        }
        _life.sizeDelta = Vector2.Lerp(_life.sizeDelta,new Vector2((life * _widthLifeMax)/lifeMax, _lifeRef.rect.height),  2);
        _lifeText.text = $"{life}/{lifeMax} HP";
    }

    public void UpdateExp(int expMax, int exp)
    {
        if (_widthExpMax==0)
        {
            _widthExpMax = _expMax.rect.width;
        }
        _exp.sizeDelta = new Vector2((exp * _widthExpMax)/expMax, _expMax.rect.height);
        _expText.text = $"Exp. {exp}/{expMax}";
    }

    public void UpdateLevelCharactr(int level)
    {
        _level.text = level.ToString();
    }
}
