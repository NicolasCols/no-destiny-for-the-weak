﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovimientoMundoAbierto: MonoBehaviour
{
    private float horizontalMov;
    private float verticalMov;
    private CharacterController player;
    private Vector3 playerInput;
    
    public float speed;
    public float gravity;
    private float fallVelocity;
    public float jumpForce;

    [SerializeField] private GameObject _characterMap;
    private bool isMoving;

    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    private Vector3 movPlayer;

    private bool charge = false;
    private Vector3 _posInitial = new Vector3(-28.8f,1.3f,-17.37f); 
    private Vector3 _posChaMap= Vector3.down * 1000;

    private  AudioSource audioSource;

    public Animator anim;
    void Start()
    {
        charge = false;
        player = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();

        isMoving = true;
        for (int i = 0; i < 2; i++)
        {
            if (PlayerPrefs.GetInt("NewGame", 1) == 1)
            {
                transform.position = _posInitial;
                _characterMap.transform.position = transform.position + _posChaMap;
                _characterMap.transform.rotation = transform.rotation;
                isMoving = false;
            }
            else 
            { 
                transform.position = SessionData.Data.quarzPos; 
                transform.rotation = SessionData.Data.quarzRot; 
                player.transform.position = transform.position; 
                _characterMap.transform.position = transform.position + _posChaMap;
                _characterMap.transform.rotation = transform.rotation;
                movPlayer = SessionData.Data.quarzMovPlayer;
            }
        }
        

        charge = true;
    }

    
    void Update()
    {
        if (!charge) return;
        if (!isMoving)
        {
            anim.SetFloat("Speed", 0);
            return;
        }
        
        horizontalMov = Input.GetAxis("Horizontal");
        verticalMov = Input.GetAxis("Vertical");
        
        playerInput = new Vector3(horizontalMov, 0, verticalMov);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

    
        
        CamDirection();

        movPlayer = playerInput.x * camRight + playerInput.z * camForward;
        movPlayer = movPlayer * speed;
        
        player.transform.LookAt(player.transform.position +  movPlayer);

        SetGravity();
        

        
       
        if (horizontalMov==0 && verticalMov==0) 
        { 
            anim.SetFloat("Speed", 0);
            audioSource.Stop();
        }
        else 
        { 
            anim.SetFloat("Speed",1);
            
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        } 
        
        
        player.Move(movPlayer  * Time.deltaTime);
        _characterMap.transform.position = transform.position + _posChaMap;
        _characterMap.transform.rotation = transform.rotation;
        
        if (Input.GetKey(KeyCode.Escape))
        {
            SessionData.Data.quarzPos = transform.position;
            SessionData.Data.quarzMovPlayer = movPlayer;
            SessionData.Data.quarzRot = transform.rotation;
            PlayerPrefs.SetInt("NewGame", 0); 
            SceneManager.LoadScene("Menu");
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    public void SetMoviment(bool mov)
    {
        isMoving = mov;
    }
    private void CamDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    private void SetGravity()
    {
        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movPlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movPlayer.y = fallVelocity;
        }
    }
    private void PlayerJump()
    {
        if (player.isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            fallVelocity = jumpForce;
            movPlayer.y = fallVelocity;
        }
    }
}
