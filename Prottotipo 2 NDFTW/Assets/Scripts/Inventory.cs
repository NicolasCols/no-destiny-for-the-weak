﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [SerializeField] private GameObject _inventory;

    [SerializeField] private GameObject _panelCharacter;

    [SerializeField] private GameObject _map;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) && !_map.activeInHierarchy)
        {
            _panelCharacter.SetActive(!_panelCharacter.activeInHierarchy);
            _inventory.SetActive(!_inventory.activeInHierarchy);
        }

        if (Input.GetKeyDown(KeyCode.M) && !_inventory.activeInHierarchy)
        {
            _panelCharacter.SetActive(!_panelCharacter.activeInHierarchy);
            _map.SetActive(!_map.activeInHierarchy);
        }
    }
    
    [SerializeField] private RectTransform _expMax;
    [SerializeField] private Text _expText;
    [SerializeField] private RectTransform _exp;
    [SerializeField] private Text _level;
    [SerializeField] private Text _attack;
    [SerializeField] private Text _denfense;
    
    private float _widthExpMax;
    public void UpdateExp(int expMax, int exp)
    {
        if (_widthExpMax==0)
        {
            _widthExpMax = _expMax.rect.width;
        }
        _exp.sizeDelta = new Vector2((exp * _widthExpMax)/expMax, _expMax.rect.height);
        _expText.text = $"EXP. {exp}/{expMax}";
    }

    public void UpdateLevelCharactr(int level)
    {
        _level.text = level.ToString();
    }
    public void UpdateAttackDefense(float attack, float defense)
    {
        _attack.text = $"Attack: {attack}";
        _denfense.text = $"Defense: {defense}";
    }
}
