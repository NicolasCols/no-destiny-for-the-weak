﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class Nuvia : MonoBehaviour
{
    // Start is called before the first frame update
    private NavMeshAgent _nv;
    
    [SerializeField] private float _life;
    [SerializeField] private float _maxLife;
    [SerializeField] private int _exp;
    [SerializeField] private int _expToNextLevel;
    [SerializeField] private int _level;
    [SerializeField] private float _force;
    [SerializeField] private float _defending;
    [SerializeField] private int _velocity;

    [SerializeField] private Transform _target;
    [SerializeField] private  bool _friend;
    [SerializeField] private float _distanceToQuarz;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject forceField;

    
    void Start()
    {
        _nv = GetComponent<NavMeshAgent>();
        _nv.enabled = false;
        Load();
    }

    void Load()
    {
        if (PlayerPrefs.GetInt("NewGame", 1) == 1)
        {
            SessionData.Data.nuviaPos = transform.position;
            SessionData.Data.nuviaRot = transform.rotation;
            SessionData.Data.nuviaLife = _life;
            SessionData.Data.nuviaMaxLife = _maxLife;
            SessionData.Data.nuviaExp = _exp;
            SessionData.Data.nuviaExpToNextLevel = _expToNextLevel;
            SessionData.Data.nuviaLevel = _level;
            SessionData.Data.nuviaForce = _force;
            SessionData.Data.nuviaDefending = _defending;
            SessionData.Data.nuviaVelocity = _velocity;
        }
        else
        {
            transform.position = SessionData.Data.nuviaPos;
            transform.rotation = SessionData.Data.nuviaRot;
            _life = SessionData.Data.nuviaLife;
            _maxLife = SessionData.Data.nuviaMaxLife;
            _exp = SessionData.Data.nuviaExp;
            _expToNextLevel = SessionData.Data.nuviaExpToNextLevel;
            _level = SessionData.Data.nuviaLevel;
            _force = SessionData.Data.nuviaForce;
            _defending = SessionData.Data.nuviaDefending;
            _velocity = SessionData.Data.nuviaVelocity;
        }

        _nv.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        _nv.stoppingDistance = _distanceToQuarz;
        if (_friend && (Vector3.Distance(transform.position, _target.position)>_distanceToQuarz))
        {
            anim.SetFloat("Speed",1);
            _nv.SetDestination(_target.position );
        }
        else
        {
            if ((Vector3.Distance(transform.position, _target.position)<=_distanceToQuarz))
            {
                anim.SetFloat("Speed",0);
            }
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            SessionData.Data.nuviaPos = transform.position;
            SessionData.Data.nuviaRot = transform.rotation;

            
        }
    }

    public void ChargeLife(float ind)
    {
        _life += ind * _maxLife;
        if (_life>_maxLife)
        {
            _life = _maxLife;
            SessionData.Data.nuviaLife = _life;
        }
    }
    public void Friend(bool win)
    {
        if (win)
        {
            _friend = true;
            Destroy(forceField);
        }
        
    }
}
