﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResultFight
{
    void FinishFight(Transform playerFight);
}
