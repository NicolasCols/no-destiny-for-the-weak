﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class GameController : MonoBehaviour
{
    public string idResult;
    
    public List<Fighter> fighters;
    
    public static GameController singlenton;
    
    //public Action proxAccion = new Action();
    public ActionsSO proxAccion;
    public TransformPlayer proxTranform;
    public string operation;
    private int _enemyes;
    private int _friends;
    private bool _win;
    private bool _lose;

    void Awake()
    {

        if (singlenton != null)
        {
            Destroy(gameObject);
            return;
        }

        singlenton = this;
    }

    void Start()
    {
        _enemyes = 0;
        _friends = 0;
        UpdateInterface();
        MetodoBurbuja();
        ChangeQueue();
        StartCoroutine(Bucle());
    }

    public Text mesaggeText;

    public Queue<Fighter> queue = new Queue<Fighter>();

    public void ChangeQueue()
    {
        foreach (var fighter in fighters)
        {
            queue.Enqueue(fighter);
            if (fighter.GetEnemy())
            {
                _enemyes++;
            }
            else
            {
                _friends++;
            }
        }
    }

    public void UpdateInterface()
    {
        mesaggeText.text = "";
        foreach (var fighter in fighters)
        {
            if (fighter.GetIsLife())
            {
                mesaggeText.text += $"{fighter.name} HP: {fighter.GetLife()}/100  VEL: {fighter.GetVelocity()}\n";
            }
        }
    }

    public Transform objetive;
    List<Button> poolBotons = new List<Button>();
    

    public bool sw= false;

    public UIController uiController;
    IEnumerator Bucle()
    {
        var fightersToRemove = new List<Fighter>();

        var turn = queue.Dequeue();
        while (!turn.GetIsLife())
        {
            if (turn.GetEnemy())
            {
                _enemyes--;
            }
            else
            {
                _friends--;
            }
            turn = queue.Dequeue();
        }

        foreach (var fighter in fighters)
        {
            if (!fighter.GetIsLife())
            {
                fightersToRemove.Add(fighter);

            }
        }

        foreach (var fighter in fightersToRemove)
        {
            fighters.Remove(fighter);
            fighter.gameObject.SetActive(false);
        }

        fightersToRemove.Clear();

        if (_enemyes==0)
        {
            PlayerPrefs.SetString(idResult,"Yes");
            SceneManager.LoadScene("MundoAbierto");
        }
        else
        {
            if (_friends==0)
            {
                PlayerPrefs.SetString(idResult,"No");
                SceneManager.LoadScene("MundoAbierto");
            }
        }
        for (int i = 0; i < poolBotons.Count; i++)
        {
            poolBotons[i].gameObject.SetActive(false);

        }

        IEnumerator c = null;
        objetive.position = Vector3.one * 999;
        int indice = 0;
        turn.EnterTurn();
        if (!turn.GetEnemy())
        { 
            uiController.Init(turn); 
            while (!sw) 
            { 
                yield return null;
            }
            
            if (operation == "Action") 
            { 
                if (!proxAccion.likewise) 
                { 
                    while (!Input.GetKey(KeyCode.Space)) 
                    { 
                        yield return null; 
                        if (Input.GetKeyDown(KeyCode.LeftArrow)) 
                        { 
                            do 
                            { 
                                indice--; 
                                if (indice < 0) {
                                    indice = fighters.Count - 1;
                                }
                            } while (!fighters[indice].GetEnemy());
                            
                            objetive.position = fighters[indice].transform.position + Vector3.up * 5;
                        }
                        if (Input.GetKeyDown(KeyCode.RightArrow))
                        { 
                            do { 
                                indice++; 
                                if (indice >= fighters.Count) 
                                { 
                                    indice = 0;
                                    
                                }
                            } while (!fighters[indice].GetEnemy());
                            
                            objetive.position = fighters[indice].transform.position + Vector3.up * 5;
                        }

                    }
                }
                
                c = turn.PlayAction(proxAccion, fighters[indice].transform);
                    turn.ExitTurn();
            }
            else 
            { 
                if (operation=="Transform") 
                { 
                    c = turn.Transformation(proxTranform);
                        turn.ExitTurn();
                }
            }
        }
        else 
        { 
            yield return new WaitForSeconds(0.5f); 
            int n = Random.Range(0, fighters.Count); 
            while (fighters[n].GetEnemy()) 
            { 
                n = Random.Range(0, fighters.Count);
            }
            
            c = turn.PlayAction(turn.actionsSO[Random.Range(0, turn.actionsSO.Count)], fighters[n].transform);
                yield return new WaitForSeconds(0.5f);
                turn.ExitTurn();
        }
        while (c == null) 
        { 
            yield return null;
        }
        yield return StartCoroutine(c);
        

        yield return new WaitForSeconds(1);

        UpdateInterface();
        
        if (turn.GetIsLife())
        {
            queue.Enqueue(turn);
        }
        
        sw = false;
        yield return Bucle();
    }


    
    public void MetodoBurbuja()
    {
        Fighter t;
        for (int a = fighters.Count; a >0; a--)
        {
            for (int b = 0; b <fighters.Count -1; b++)
            {
                if (fighters[b + 1].GetVelocity() > fighters[b].GetVelocity())
                {
                    t = fighters[b + 1];
                    fighters[b + 1] = fighters[b];
                    fighters[b] = t;
                }
            }
        }
    }
}

