﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObjet : MonoBehaviour
{
    public GameObject ObjetToPickUp;
    public GameObject PickedObjet; //Objeto que hemos cogido
    public Transform InteractionZone;
    
    void Update()
    {
        if ( ObjetToPickUp!=null && ObjetToPickUp.GetComponent<PickableObjet>().isPickable && PickedObjet==null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PickedObjet = ObjetToPickUp;
                PickedObjet.GetComponent<PickableObjet>().isPickable = false;
                PickedObjet.transform.SetParent(InteractionZone);
                PickedObjet.transform.position = InteractionZone.position;
                PickedObjet.transform.rotation = InteractionZone.rotation;
                PickedObjet.GetComponent<Rigidbody>().isKinematic = true;
                PickedObjet.GetComponent<Rigidbody>().useGravity = false;
            }
        }
        else if(PickedObjet != null )
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PickedObjet.GetComponent<PickableObjet>().isPickable = true;
                PickedObjet.transform.SetParent(null);
                PickedObjet.GetComponent<Rigidbody>().isKinematic = false;
                PickedObjet.GetComponent<Rigidbody>().useGravity = true;
                PickedObjet = null;
            }
        }
    }
}
