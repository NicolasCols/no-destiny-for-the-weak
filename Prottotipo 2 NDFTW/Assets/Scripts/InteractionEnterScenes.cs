﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractionEnterScenes : MonoBehaviour
{
    [SerializeField]private  string scene;
    [SerializeField] private bool _dialog;

    private void OnTriggerStay(Collider other)
    {

        if (other.tag == "PlayerInteractionZone" && Input.GetKeyDown(KeyCode.E) && !_dialog)
        {
            EnterScene();
        }
    }

    public void EnterScene()
    {
        PlayerPrefs.SetInt("NewGame",0);
        var player = GameObject.FindWithTag("Player").transform;
        SessionData.Data.quarzPos = player.position;
        SessionData.Data.quarzRot = player.rotation;
        SceneManager.LoadScene(scene);
    }
}
