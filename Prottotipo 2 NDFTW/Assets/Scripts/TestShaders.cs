﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class TestShaders : MonoBehaviour
{
    [SerializeField]private Material materialTranform;
    [SerializeField] private Material materialOriginal;
    [SerializeField] private Material materialfinal;
    
    [SerializeField] private Shader _shader;
    private string id = "Vector1_304CDB40";
    private string idColor = "Color_A05F5DDE";
    private MeshRenderer rendeerer;
    // Start is called before the first frame update
    void Start()
    {
        rendeerer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(Transformable());

        }
    }

    IEnumerator Transformable()
    {
        float ind = 0;
        rendeerer.material = materialTranform;
        materialTranform.SetColor(idColor,materialOriginal.color);
        while (ind<Math.PI)
        {
            ind += 0.01f;
            materialTranform.SetFloat(id,ind);
            yield return new WaitForSeconds(0.01f);
        }
        
        materialTranform.SetColor(idColor, materialfinal.color);
        
        while (ind<2*Math.PI)
        {
            ind += 0.01f;
            materialTranform.SetFloat(id,ind);
            yield return new WaitForSeconds(0.01f);
        }
        rendeerer.material = materialfinal;

    }
}
