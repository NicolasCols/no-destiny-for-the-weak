﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quarz : MonoBehaviour
{
    [SerializeField] private float _life;
    [SerializeField] private float _maxLife;
    [SerializeField] private int _exp;
    [SerializeField] private int _expToNextLevel;
    [SerializeField] private int _level;
    [SerializeField] private float _force;
    [SerializeField] private float _defending;
    [SerializeField] private int _velocity;
    [SerializeField] private string _keys;

    [SerializeField]private UICharacterWorld _uiCharacter;
    [SerializeField]private Inventory _inventory;


    
    // Start is called before the first frame update
    void Start()
    {
        Load();
        
    }


    void Load()
    {
        if (PlayerPrefs.GetInt("NewGame",1)==1)
        {
            SessionData.Data.quarzLife = _life;
            SessionData.Data.quarzMaxLife = _maxLife;
            SessionData.Data.quarzExp = _exp;
            SessionData.Data.quarzExpToNextLevel = _expToNextLevel;
            SessionData.Data.quarzLevel = _level;
            SessionData.Data.quarzForce = _force;
            SessionData.Data.quarzDefending = _defending;
            SessionData.Data.quarzVelocity = _velocity;
        }
        else
        {
            _life = SessionData.Data.quarzLife;
            _maxLife = SessionData.Data.quarzMaxLife;
            _exp = SessionData.Data.quarzExp;
            _expToNextLevel = SessionData.Data.quarzExpToNextLevel;
            _level = SessionData.Data.quarzLevel;
            _force = SessionData.Data.quarzForce;
            _defending = SessionData.Data.quarzDefending;
            _velocity = SessionData.Data.quarzVelocity;
        }

        UpdateUI();
    }

    void UpdateUI()
    {
        _uiCharacter.UpdateLife(_life,_maxLife);
        
        _inventory.UpdateExp(_expToNextLevel, _exp);
        _uiCharacter.UpdateExp(_expToNextLevel, _exp);
        
        _inventory.UpdateLevelCharactr(_level);
        _uiCharacter.UpdateLevelCharactr(_level);
        
        _inventory.UpdateAttackDefense(_force,_defending);
    }
    public void ChargeLife(float ind)
    {
        _life += ind * _maxLife;
        if (_life>_maxLife)
        {
            _life = _maxLife;
            SessionData.Data.quarzLife = _life;
        }
        
        UpdateUI();
    }

    public void IncreaseLifeMax(float cant)
    {
        _maxLife += cant;
        _life = _maxLife;
        SessionData.Data.quarzLife = _life;
        SessionData.Data.quarzMaxLife = _maxLife;
        UpdateUI();
    }

    public void IncreaseLevel(int level=1)
    {
        _level += level;
        SessionData.Data.quarzLevel = _level;
        UpdateUI();
    }
    public void IncreaseExp(int exp)
    {
        _exp += exp;
        if (_exp>= _expToNextLevel)
        {
            IncreaseLevel();
            _exp -= _expToNextLevel;
            _expToNextLevel += _expToNextLevel;
            
            UpdateUI();
        }
        SessionData.Data.quarzExp = _exp;
        SessionData.Data.quarzExpToNextLevel= _expToNextLevel;
        _uiCharacter.UpdateExp(_expToNextLevel,_exp);
    }
    public void Mask(float ind= 0.1f)
    {
        _defending += ind;
        SessionData.Data.quarzDefending = _defending;
        UpdateUI();
        Debug.Log(_defending +"Defensa");
    }
    public void Claw(float force = 0.1f)
    {
        _force+= force;
        SessionData.Data.quarzForce = _force;
        UpdateUI();
        Debug.Log("Fuerza: "+_force);
    }
    public void Key01()
    {
        Debug.Log("Esta no es la llave de esta puerta");
    }
}
