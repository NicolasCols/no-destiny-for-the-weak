﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/InventoryData", order = 1)]

public class InventorySO : ScriptableObject
{
    [System.Serializable]
    public struct InventoryBaseData
    {
        public string name;
        public Sprite sprite;
        public Use use;
        [TextArea(3,10)]
        public string caracteristic;
        public string funcion;
        public float value;
        public bool equipped;
    }

    public enum Use
    { 
        KConsumable, 
        KEquippable, 
        Kusable
    }

    public InventoryBaseData[] baseData;
}

