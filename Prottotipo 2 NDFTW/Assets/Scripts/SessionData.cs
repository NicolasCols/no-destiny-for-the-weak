﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class SessionData {

	private static GameData GAME_DATA;

	private static bool LoadData() {
        var valid = false;

        var data = PlayerPrefs.GetString("data", "");
        if (data != "") {
	        var success = DESEncryption.TryDecrypt(data, out var original);
	        if (success) {
		        GAME_DATA = JsonUtility.FromJson<GameData>(original);
		        GAME_DATA.LoadData();
		        valid = true;    
	        }
	        else {
		        GAME_DATA = new GameData();
	        }
            
        } else {
            GAME_DATA = new GameData();
        }

        return valid;
    }

    public static bool SaveData() {
        const bool valid = false;

        try {
            GAME_DATA.SaveData();
            var result = DESEncryption.Encrypt(JsonUtility.ToJson(SessionData.GAME_DATA));
            PlayerPrefs.SetString("data", result);
            PlayerPrefs.Save();
        } catch (Exception ex) {
            Debug.LogError(ex.ToString());
        }

        return valid;
    }

    public static GameData Data {
        get {
			if (GAME_DATA == null)
                LoadData();
            return GAME_DATA;
		}
    }

}


[Serializable]
public class GameData {
	//Put attributes that you want to save during your game.
	//Quarz
	public float quarzLife;
	public float quarzMaxLife;
	public int quarzExp;
	public int quarzExpToNextLevel;
	public int quarzLevel;
	public float quarzForce;
	public float quarzDefending;
	public int quarzVelocity;
	
	//posicion de quarz 
	public Vector3 quarzPos;
	public Quaternion quarzRot;
	public Vector3 quarzMovPlayer;

	//Nuvia
	public float nuviaLife;
	public float nuviaMaxLife;
	public int nuviaExp;
	public int nuviaExpToNextLevel;
	public int nuviaLevel;
	public float nuviaForce;
	public float nuviaDefending;
	public int nuviaVelocity;
	
	//PosNuvia
	public Vector3 nuviaPos;
	public Quaternion nuviaRot;

	//Inventory
    	public List<InventoryManager.ObjectInventoryID> invInventory;
    	public List<InventoryManager.ObjectInventoryID> invInventoryEquipable;
	
	public GameData() {

		
	}

	public void SaveData() {
    
				
    }

	public void LoadData() {
	
	}
}