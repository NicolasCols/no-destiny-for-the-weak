﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public struct TransformPlayer
{
    public string tipo;
    public GameObject prefab;
    public Texture2D texture;
    public Texture2D textureMetalic;
    public Material material;
    public List<ActionsSO> actions;
    public SkinnedMeshRenderer meshRenderer;
    public Animator animatorTransform;
}



public class Fighter : MonoBehaviour
{

    [Header("caracterist")]
    public List<ActionsSO> actionsSO;
    
    
    [SerializeField] private GameObject patricleObjet;
  //  [SerializeField] private Camera _camera;
    
    [Header("Character")]
    [SerializeField] private string name;
    [SerializeField] private string type;
    [SerializeField] private float life;
    [SerializeField] private int velocity;
    [SerializeField] private bool enemy;
    [SerializeField] private bool isLife;
    [SerializeField] private bool isTransform;
    
    public List<TransformPlayer> transformPlayers;
    
    private string _idTexture2D = "Texture2D_F8179555";
    private string _idMetalicTexture = "Texture2D_C8B06338";
    private string _idTime = "Vector1_AD6CBD48";
    private float _maxLife;
    private float maxLifeWidth;
    [SerializeField]private RectTransform _rectLifeTransform;
    [SerializeField] private Animator anim;
    
    [Header("If it Tranforms")]
    
    [SerializeField] private GameObject model;
    [SerializeField] private SkinnedMeshRenderer _renderer;
    [SerializeField] private Material _materialTransform;
    
    void ChangeLife(int cant)
    {
        life += cant;
        if (name =="Quarz")
        {
            SessionData.Data.quarzLife = life;
        }
        _gameController.UpdateInterface();
        
        _rectLifeTransform.sizeDelta =new Vector2((life * maxLifeWidth) / _maxLife,0.37f); 
    }
    
    void ChangeVelocity(int cant)
    {
        velocity += cant;
        _gameController.UpdateInterface();
    }

    public float GetLife()
    {
        return life;
    }

    public int GetVelocity()
    {
        return velocity;
    }

    public bool GetIsLife()
    {
        return isLife;
    }

    public bool GetEnemy()
    {
        return enemy;
    }

    public bool GetTransform()
    {
        return isTransform;
    }
     
    private NavMeshAgent nv;
    private GameController _gameController;

    
    void Start()
    {
        _gameController = GameController.singlenton;
        nv = GetComponent<NavMeshAgent>();
        nv.updateRotation = false;
        _maxLife = life;
        if (name == "Quarz")
        {
            life = SessionData.Data.quarzLife;
            _maxLife = SessionData.Data.quarzMaxLife;
            Debug.Log("Vida de Quarz: " + life);
        }
        
        
        maxLifeWidth = _rectLifeTransform.rect.width;
        if (isTransform)
        {
            model.SetActive(true);
        }
    }

    public IEnumerator PlayAction(ActionsSO action, Transform objetive)
    {
        
        if (action.likewise)
        {
            objetive = transform;
        }

        if (action.particle)
        {
            if (!action.staticParticle)
            {
                patricleObjet.transform.position = objetive.transform.position;
            }
            else
            {
                patricleObjet.transform.position = transform.position;
                patricleObjet.transform.LookAt(objetive);
            }
            action.particleSystem.Play();
        }

        if (action.isShader)
        {
            //SHADER attack
        }
        if (action.estatic)
        {
            anim.SetTrigger(action.animationTrigger);
            yield return new WaitForSeconds(action.timeAction);
            objetive.SendMessage(action.mensagge,action.argument);
        }
        else
        {
            Vector3 posIni = transform.position;
            Quaternion rotIni = transform.rotation;
            
            transform.LookAt(objetive.transform.position);
            nv.SetDestination(objetive.position);
            anim.SetFloat("Speed",1);
            
            while (Vector3.Distance(transform.position, objetive.position)>3)
            {
                yield return null;
                
            }
            anim.SetFloat("Speed",0);
            nv.speed = 0;
            if (action.particle)
            {
                action.particleSystem.Play();
            }
            
            anim.SetTrigger(action.animationTrigger);
            yield return new WaitForSeconds(action.timeAction);
            
            objetive.SendMessage(action.mensagge,action.argument);
            
            yield return new WaitForSeconds(1);
            
            transform.LookAt(posIni);
            nv.SetDestination(posIni);
            nv.speed = 4.5f;
            anim.SetFloat("Speed",1);
            while (Vector3.Distance(transform.position, posIni)>0.1f)
            {
                yield return null;
            }
            anim.SetFloat("Speed",0);
            transform.rotation = rotIni;
        }

        if (action.particle)
        {
            action.particleSystem.Stop();
        }
    }

    public IEnumerator Transformation(TransformPlayer tranformation)
    {
        float ind = 0;

        var modelmat = _renderer.material;
        _materialTransform.SetTexture(_idTexture2D, _renderer.material.mainTexture);
        

        _renderer.material = _materialTransform;
                 
                 
                 while (ind< Mathf.PI)
                 {
                     ind += 0.05f;
                     _materialTransform.SetFloat(_idTime,ind);
                     yield return new WaitForSeconds(0.01f);
                 }
        model.SetActive(false);
        _renderer.material = modelmat;
        _renderer = tranformation.meshRenderer;
        tranformation.meshRenderer.material = _materialTransform;
        model = tranformation.prefab;
        model.SetActive(true);
        _materialTransform.SetTexture(_idTexture2D,tranformation.texture);
        _materialTransform.SetTexture(_idMetalicTexture, tranformation.textureMetalic);

        while (ind<2*Mathf.PI)
        {
            ind += 0.05f;
            _materialTransform.SetFloat(_idTime,ind);
            yield return new WaitForSeconds(0.01f);
        }

        tranformation.meshRenderer.material = tranformation.material;
        yield return new WaitForSeconds(1);
        actionsSO = tranformation.actions;
        anim = tranformation.animatorTransform;
        type = tranformation.tipo;
    }
    
    void Update()
    {
        if (life<=0)
        {
            isLife = false;
            gameObject.SetActive(false);
            Destroy(_rectLifeTransform.transform.parent.gameObject);
            if (name=="Quarz")
            {
                SessionData.Data.quarzLife = 1;
            }
        }
        
    }
    

    public void EnterTurn()
    {
        _rectLifeTransform.transform.parent.gameObject.SetActive(true);
    }

    public void ExitTurn()
    {
        _rectLifeTransform.transform.parent.gameObject.SetActive(false);
    }

    void OnMouseOver()
    {
        _rectLifeTransform.transform.parent.gameObject.SetActive(true);
    }

    void OnMouseExit()
    {
        _rectLifeTransform.transform.parent.gameObject.SetActive(false);
    }
}
