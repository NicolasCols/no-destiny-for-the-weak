﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCollider : MonoBehaviour
{
    [SerializeField] private InventoryManager _inventoryManager;

    public void OnTriggerStay(Collider col)
    {
        if (col.CompareTag("InventoryObject") && Input.GetKeyDown(KeyCode.E))
        {
            InventoryObject inventoryObject = col.GetComponent<InventoryObject>();
            if (inventoryObject != null)
            {
                _inventoryManager.AddInInventory(inventoryObject.id,inventoryObject.amount);
                inventoryObject.GrabObject();
            }
            else
            {
                Debug.Log("InventoryObject not exist");
            }
        }
    }

}
