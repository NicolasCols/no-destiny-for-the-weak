﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    public Transform panel;
    public Button prefabBotton;
    List<Button> poolBotons = new List<Button>( );
    public string[] interaction;

    void Start()
    {
        
    }

    public void Init(Fighter fighter)
    {
        poolBotons.Clear();
        int indice = 2;
        for (int i = 0; i < indice; i++)
        {
            Button b = null;
            for (int j = 0; j < poolBotons.Count; j++)
            { 
                if (!poolBotons[j].gameObject.activeInHierarchy) 
                { 
                    b = poolBotons[j];
                    Destroy(poolBotons[j].gameObject);
                    poolBotons.Remove(poolBotons[j]);
                    
                }
            }
            
            b = Instantiate(prefabBotton, panel);

            b.transform.position = Vector3.zero;
            b.transform.localScale = Vector3.one;

            poolBotons.Add(b);

            b.gameObject.SetActive(true);
            b.onClick.RemoveAllListeners();

            b.GetComponentInChildren<Text>().text = interaction[i];

            b.interactable = true;
        }
        poolBotons[0].onClick.AddListener((() => 
                Attack(fighter)
                ));
        
        if (fighter.GetTransform())
        {
            Button b = null;
            b = Instantiate(prefabBotton, panel);
            b.transform.position = Vector3.zero; 
            b.transform.localScale = Vector3.one;
            poolBotons.Add(b);
            b.gameObject.SetActive(true); 
            b.onClick.RemoveAllListeners();
            b.GetComponentInChildren<Text>().text = "Transforms";
            b.interactable = true;
            poolBotons[2].onClick.AddListener((() => 
                    Transform(fighter)
                    ));
        }
        poolBotons[1].onClick.AddListener((() => 
                Escape()
            ));
        
        

    }

    public void Transform(Fighter fighter)
    {
        for (int i = 0; i < poolBotons.Count; i++)
        {
            poolBotons[i].gameObject.SetActive(false);
        }

        foreach (var accion in fighter.transformPlayers)

        {
            Button b = null;
            for (int i = 0; i < poolBotons.Count; i++)
            {
                if (!poolBotons[i].gameObject.activeInHierarchy)
                {
                    b = poolBotons[i];

                    Destroy(poolBotons[i].gameObject);
                    poolBotons.Remove(poolBotons[i]);
                }
            }

            b = Instantiate(prefabBotton, panel);

            b.transform.position = Vector3.zero;
            b.transform.localScale = Vector3.one;

            poolBotons.Add(b);

            b.gameObject.SetActive(true);
            b.onClick.RemoveAllListeners();

            b.GetComponentInChildren<Text>().text = accion.tipo;

            b.interactable = true;
            b.onClick.AddListener(() =>
            {
                for (int j = 0; j < poolBotons.Count; j++)
                {
                    poolBotons[j].gameObject.SetActive(false);
                }
                _gameController.proxTranform = accion;
                _gameController.sw= true;
                _gameController.operation = "Transform";
            }); 
        }
        
    }

    public void Attack(Fighter figther)
    {
        for (int i = 0; i < poolBotons.Count; i++)
        {
            poolBotons[i].gameObject.SetActive(false);

        }
        foreach (var accion in figther.actionsSO)
        {
            Button b = null;
            for (int i = 0; i < poolBotons.Count; i++)
            {
                if (!poolBotons[i].gameObject.activeInHierarchy)
                {
                    b = poolBotons[i];

                    Destroy(poolBotons[i].gameObject);
                    poolBotons.Remove(poolBotons[i]);
                }
            }

            b = Instantiate(prefabBotton, panel);

            b.transform.position = Vector3.zero;
            b.transform.localScale = Vector3.one;

            poolBotons.Add(b);

            b.gameObject.SetActive(true);
            b.onClick.RemoveAllListeners();

            b.GetComponentInChildren<Text>().text = accion.name;

            b.interactable = true;
            b.onClick.AddListener(() =>
            {
                for (int j = 0; j < poolBotons.Count; j++)
                {
                    poolBotons[j].gameObject.SetActive(false);
                }

                _gameController.proxAccion = accion;
                _gameController.sw= true;
                _gameController.operation = "Action";
            });
        }
    }

    public void Escape()
    {
        for (int i = 0; i < poolBotons.Count; i++)
        {
            poolBotons[i].gameObject.SetActive(false);

        }
        
        SceneManager.LoadScene("MundoAbierto");
    }
        
}
