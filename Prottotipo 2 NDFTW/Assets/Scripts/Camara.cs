﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Camara : MonoBehaviour
{


   public Vector3 offset;
   private Vector3 offsetInit;
   public Transform target;
   [Range(0, 1)] public float lerpValue;
   public float sensibilidad;
   
   public float distance;

   void Start()
    {
        target = GameObject.Find("Player").transform;
    }


    void LateUpdate()
    {
        
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * sensibilidad, Vector3.up) * offset  ;
       // Vector3 raycastDir = offset;
       var lookPosition= target.position + offset;
        /*if (Physics.Raycast(target.position,raycastDir, out RaycastHit hit, distance)) {
            if (hit.collider.tag == "Pared")
            {
                lookPosition = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * sensibilidad, Vector3.up) *hit.point;
            }
        }
        */
        transform.position = Vector3.Lerp(transform.position,lookPosition,lerpValue);
        

        transform.LookAt(target);


    }
}
