﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    [System.Serializable]
    public struct ObjectInventoryID
    {
        public int id;
        public int ammount;

        public ObjectInventoryID(int id, int ammount)
        {
            this.id = id;
            this.ammount = ammount;
        }
        
    }

    public InventorySO baseData;
    public List<ObjectInventoryID> inventory;

    void Start()
    {
        Load();
    }

    void Load()
    {
        if (PlayerPrefs.GetInt("NewGame",1)==1)
        {
            SessionData.Data.invInventory = inventory;
            SessionData.Data.invInventoryEquipable = inventoryEquipable;
        }
        else
        {
            inventory = SessionData.Data.invInventory;
            inventoryEquipable = SessionData.Data.invInventoryEquipable;
        }
        Debug.Log(inventory.Count);
        Debug.Log(inventoryEquipable.Count);
        UpdateInventory(_inventaryUi,pool,inventory);
        UpdateInventory(_equipamentUi,_poolEquipable,inventoryEquipable);
    }
    public void AddInInventory(int id, int ammount)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].id == id)
            {
                inventory[i] = new ObjectInventoryID(inventory[i].id, inventory[i].ammount + ammount);
                UpdateInventory(_inventaryUi,pool,inventory);
                return;
            }
        }
        inventory.Add(new ObjectInventoryID(id,ammount));
        UpdateInventory(_inventaryUi,pool,inventory);
    }

    public void DeleteInInventory(int id, int ammount)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].id == id)
            {
                inventory[i] = new ObjectInventoryID(inventory[i].id, inventory[i].ammount - ammount);
                if (inventory[i].ammount<=0)
                {
                    inventory.Remove(inventory[i]);
                }
                UpdateInventory(_inventaryUi,pool,inventory);
                return;
            }
        }
        AddInInventory(id,ammount);
        Debug.Log("Object to delete not exist");
    }

    
    [SerializeField] private InventoryObjectToInterface _prefab;
    [SerializeField] private Transform _inventaryUi;
    [SerializeField] private Transform _equipamentUi;
    
    List<InventoryObjectToInterface> pool = new List<InventoryObjectToInterface>();

    public void UpdateInventory(Transform panel,List<InventoryObjectToInterface> poolObjectToInterfaces, List<ObjectInventoryID> inventoryIds)
    {
        for (int i = 0; i < poolObjectToInterfaces.Count; i++)
        {
            if (i < inventoryIds.Count)
            {
                ObjectInventoryID o = inventoryIds[i];
                poolObjectToInterfaces[i].sprite.sprite = baseData.baseData[o.id].sprite;
                poolObjectToInterfaces[i]._ammount.text = o.ammount.ToString();
                poolObjectToInterfaces[i].caracteristics.text = baseData.baseData[o.id].caracteristic;
                
                poolObjectToInterfaces[i].button.onClick.RemoveAllListeners();
                poolObjectToInterfaces[i].button.onClick.AddListener(() => gameObject.SendMessage("InteractInventory",o.id, SendMessageOptions.DontRequireReceiver));
                poolObjectToInterfaces[i].gameObject.SetActive(true);
            }
            else
            {
                poolObjectToInterfaces[i].gameObject.SetActive(false);
            }
        }

        if (inventoryIds.Count > poolObjectToInterfaces.Count)
        {

            for (int i = poolObjectToInterfaces.Count; i < inventoryIds.Count; i++)
            {
                InventoryObjectToInterface oi = Instantiate(_prefab, panel);
                poolObjectToInterfaces.Add(oi);

                oi.transform.position = Vector3.zero;
                oi.transform.localPosition = Vector3.one;

                ObjectInventoryID o = inventoryIds[i];
                poolObjectToInterfaces[i].sprite.sprite = baseData.baseData[o.id].sprite;
                poolObjectToInterfaces[i]._ammount.text = o.ammount.ToString();
                poolObjectToInterfaces[i].caracteristics.text = baseData.baseData[o.id].caracteristic;
                
                poolObjectToInterfaces[i].button.onClick.RemoveAllListeners();
                poolObjectToInterfaces[i].button.onClick.AddListener(() => gameObject.SendMessage("InteractInventory",o.id, SendMessageOptions.DontRequireReceiver));
                poolObjectToInterfaces[i].gameObject.SetActive(true);
            }
        }

        SessionData.Data.invInventory = inventory;
        SessionData.Data.invInventoryEquipable = inventoryEquipable;
    }

    List<InventoryObjectToInterface> _poolEquipable = new List<InventoryObjectToInterface>();
    public List<ObjectInventoryID> inventoryEquipable;

    private void EquiparObject(int id)
    {
        for (int i = 0; i < inventoryEquipable.Count; i++)
        {
            if (inventoryEquipable[i].id == id && baseData.baseData[id].equipped )
            {
                inventoryEquipable.Remove(inventoryEquipable[i]);
                UpdateInventory(_equipamentUi,_poolEquipable,inventoryEquipable);
                AddInInventory(id,1);
                gameObject.SendMessage(baseData.baseData[id].funcion, -baseData.baseData[id].value,
                    SendMessageOptions.DontRequireReceiver);
                baseData.baseData[id].equipped = false;
                return;
            }
        }
        
        DeleteInInventory(id,1);
        inventoryEquipable.Add(new ObjectInventoryID(id,1));
        gameObject.SendMessage(baseData.baseData[id].funcion, baseData.baseData[id].value,
            SendMessageOptions.DontRequireReceiver);
        baseData.baseData[id].equipped= true;
        UpdateInventory(_equipamentUi,_poolEquipable,inventoryEquipable);
    }

    public void InteractInventory(int id)
    {
        Debug.Log("koka");
        if (baseData.baseData[id].use == InventorySO.Use.KEquippable)
        {
            EquiparObject(id);
          //  Debug.Log("koka");
        }
        else
        {
            gameObject.SendMessage(baseData.baseData[id].funcion, baseData.baseData[id].value,
                SendMessageOptions.DontRequireReceiver);

            if (baseData.baseData[id].use != InventorySO.Use.Kusable)
            { 
                DeleteInInventory(id,1);
            }
        }
        
    }
}
