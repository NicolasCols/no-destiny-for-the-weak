﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableObjet : MonoBehaviour
{
    public bool isPickable = true;
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "PlayerInteractionZone")
        {
            other.GetComponentInParent<PickUpObjet>().ObjetToPickUp = this.gameObject;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "PlayerInteractionZone")
        {
            other.GetComponentInParent<PickUpObjet>().ObjetToPickUp = null;
        }
    }
}
