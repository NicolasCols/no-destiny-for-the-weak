﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{

    [SerializeField] private string _idChest;
    [SerializeField] private GameObject[] _prefabs;
    
    private WaitForSeconds _twoSecond = new WaitForSeconds(2);
    private WaitForSeconds _delay = new WaitForSeconds(0.2f);
    private Animator _animator;
    private AudioSource _audio;
    private string _openState = "Open";
    void Start()
    {
        _animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
        if (PlayerPrefs.GetInt(_idChest,0)==1)
        {
            _animator.SetTrigger(_openState);
            gameObject.tag = "Untagged";
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("PlayerInteractionZone") && Input.GetKeyDown(KeyCode.E) && gameObject.tag != "Untagged")
        {
            _animator.SetTrigger(_openState);
            
            _audio.Play();
            StartCoroutine(OpenChest());

            PlayerPrefs.SetInt(_idChest,1);
            gameObject.tag = "Untagged";
        }
    }


    IEnumerator OpenChest()
    {
        yield return _twoSecond;
        if (_prefabs.Length>0 && _prefabs != null)
        {
            for (int i = 0; i < _prefabs.Length; i++)
            {
                _prefabs[i].SetActive(true);
                var rb = _prefabs[i].GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                    rb.useGravity = true;
                    rb.velocity = Vector3.up * 4 + Vector3.forward * 3;
                    yield return _delay;
                }
            }
        }
    }
}
