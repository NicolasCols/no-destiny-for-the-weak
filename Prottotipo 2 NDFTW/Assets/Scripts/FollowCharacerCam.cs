﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCharacerCam : MonoBehaviour
{
    public GameObject playerMap;

    // Update is called once per frame
    void Update()
    {
        var playerPos = playerMap.transform.position;
        playerPos.y = transform.position.y;
        transform.position = playerPos;
    }
}
