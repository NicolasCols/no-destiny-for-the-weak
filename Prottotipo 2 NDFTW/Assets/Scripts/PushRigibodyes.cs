﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushRigibodyes : MonoBehaviour
{
    public float pushForce;
    private float targetMass;
    
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        if (body== null || body.isKinematic)
        {
            return;
        }

        if (hit.moveDirection.y <-0.3f)
        {
            return;
        }

        targetMass = body.mass;
        
        Vector3 pushDirection = new Vector3(hit.moveDirection.x,0,hit.moveDirection.y);

        body.velocity = pushDirection * pushForce / targetMass;

    }
}
