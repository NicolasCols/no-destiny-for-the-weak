﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    //Editors members
    [SerializeField] private Dialogue _dialogues;
    [SerializeField] private GameObject _panelDialog;
    [SerializeField] private Text _textPanel;

    [SerializeField] private float _speedDialog;

    [SerializeField] private bool startFight;
    [SerializeField] private InteractionEnterScenes fight;
    
    
    [SerializeField] private string conditionToStart;
    [SerializeField] private string idDialog;
    [SerializeField] private int _idDialog;
    
    //PrivateMembers
    private string _activeSentence;
    private MovimientoMundoAbierto playerScript;
    private Queue<string> _sentences;
    private bool _startDialog;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _sentences = new Queue<string>();
        
        //SOLUCION TEMPORAL
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<MovimientoMundoAbierto>();
        
        if (PlayerPrefs.GetString(conditionToStart) == "Yes"&& PlayerPrefs.GetString(idDialog,"")!= "No")_startDialog = true;
        if (_startDialog)StartDialogue();
    }


    void StartDialogue()
    {
        _panelDialog.SetActive(true);
        _sentences.Clear();
        foreach (var sentence in _dialogues.dialogue)
        {
            _sentences.Enqueue(sentence);
        }
        NextSentence();
    }

    void LateUpdate()
    {
        if (_startDialog && (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space) )&& _textPanel.text == _activeSentence)
        {
            NextSentence();
        }
    }
    void NextSentence()
    {
        if (_sentences.Count<=0)
        {
            PlayerPrefs.SetString(idDialog,"No");
            playerScript.SetMoviment(true);
            _startDialog = false;
            _textPanel.text = _activeSentence;
            _panelDialog.SetActive(false);
            if (startFight)
            {
                fight.EnterScene();
            }
            return;
        }
        _activeSentence = _sentences.Dequeue();
        _textPanel.text = _activeSentence;
        
        StopAllCoroutines();
        //StartCoroutine(TypeTheSentence(_activeSentence));
    }

    IEnumerator TypeTheSentence(string sentence)
    {
        _textPanel.text = "";

        foreach (var letter in sentence.ToCharArray())
        {
            _textPanel.text += letter;
            yield return new WaitForSeconds(_speedDialog);
        }
    }
    // Update is called once per frame
   
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "PlayerInteractionZone" && Input.GetKeyDown(KeyCode.E))
        {
            _startDialog = true;
            playerScript.SetMoviment(false);
            StartDialogue();
        }
        
    }
}
