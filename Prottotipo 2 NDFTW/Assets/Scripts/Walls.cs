﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Walls : MonoBehaviour
{
    
    [SerializeField]private bool interaction;
    [SerializeField]private Renderer _renderer;
    [SerializeField]private Material _materialDessapear;
    [SerializeField] private string _id;
    [SerializeField] private GameObject _panelDialogue;
    [SerializeField] private Text _dialegue;
    private string _idTexture2D = "Texture2D_F8179555";
    private string _idMetalicTexture = "Texture2D_C8B06338";
    private string _idTime = "Vector1_AD6CBD48";

    void Start()
    {
        interaction = false;
        _renderer = GetComponent<Renderer>();
        if (PlayerPrefs.GetString("Arena1","")=="Yes")
        {
            interaction = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "PlayerInteractionZone" && Input.GetKeyDown(KeyCode.E))
        {
            if (interaction)
            {
                StartCoroutine(Dessapear());
                PlayerPrefs.SetString(_id, "Yes");
                interaction = false;
            }
            else
            {
               StartCoroutine( StartDialogue());
            }
            
        }
        
    }
    public void DesableWalls(bool desble)
    {
        if (desble)
        {
            interaction= true;
        }
    }


    private IEnumerator Dessapear()
    {
        float ind = 0;
        
        _materialDessapear.SetTexture(_idTexture2D, _renderer.material.mainTexture);
        _renderer.material = _materialDessapear;
                 
                 
        while (ind< Mathf.PI)
        {
            ind += 0.01f;
            _materialDessapear.SetFloat(_idTime,ind);
            yield return new WaitForSeconds(0.01f);
        }

        gameObject.SetActive(false);
        
    }

    /*public void DesableWall()
    {
        if (interaction)
        {
            StartCoroutine(Dessapear());
            PlayerPrefs.SetString(_id, "Yes");
            interaction = false;
        }
        else
        {
            //Decir q no se puede
        }
        
    }*/
    IEnumerator StartDialogue()
    {
        _panelDialogue.SetActive(true);
        _dialegue.text = "I can´t pass";
        yield return new WaitForSeconds(3);
        _panelDialogue.SetActive(false);

    }
}
