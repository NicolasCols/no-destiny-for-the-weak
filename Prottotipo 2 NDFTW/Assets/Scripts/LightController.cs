﻿using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

public class LightController : MonoBehaviour
{
    
    public void TurnOnLight(Light light)
    {
        light.enabled = true;
    }
    
    public void TurnOffLight(Light light)
    {
        light.enabled = false;
    }
    
}
