﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomMove : MonoBehaviour
{
    private NavMeshAgent nv;
    // Start is called before the first frame update
    void Start()
    {
        nv = GetComponent<NavMeshAgent>();
        StartCoroutine(MovetoSpace());
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    
    private void OnTriggerStay(Collider other)
    {
       
        if (other.tag == "Player" )
        {
            nv.SetDestination(other.transform.position);
        }
    }

    IEnumerator MovetoSpace()
    {
        Vector3 space= new Vector3(Random.Range(-40,40),0,Random.Range(-40,40));

        nv.SetDestination(space);
        yield return new WaitForSeconds(8);
        yield return StartCoroutine(MovetoSpace());
    }
}
