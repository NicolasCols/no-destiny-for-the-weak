﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DessapearToScene : MonoBehaviour
{
    [SerializeField] private string conditionToDessapear;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetString(conditionToDessapear,"")== "Yes")
        {
            gameObject.SetActive(false);
        }
    }
    
}
