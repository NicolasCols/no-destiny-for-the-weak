﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryObject : MonoBehaviour
{
    public int id;
    public int amount;

    [SerializeField] private string _idUnico;
    
    void Start()
    {
        if (PlayerPrefs.GetInt($"{_idUnico} IDObject", 0) ==1)
        {
            GrabObject();
        }
    }
    public void GrabObject()
    {
        PlayerPrefs.SetInt($"{_idUnico} IDObject", 1);
        Destroy(gameObject);
    }
}
