﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    private GameObject _nuvia;
    private GameObject _quarz;
    private Quarz _quarzScript;
    private Nuvia _nuviaScript;

    void Start()
    {
        _nuvia = GameObject.FindGameObjectWithTag("Nuvia").gameObject;
        _quarz = GameObject.FindGameObjectWithTag("Player").gameObject;
        _quarzScript = _quarz.GetComponent<Quarz>();
        _nuviaScript = _nuvia.GetComponent<Nuvia>();
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.tag == "PlayerInteractionZone" && Input.GetKeyDown(KeyCode.E))
        {
            PlayerPrefs.SetInt("NewGame",0);
            SessionData.Data.quarzPos = _quarz.transform.position;
            SessionData.Data.nuviaPos = _nuvia.transform.position;
            _quarzScript.ChargeLife(1000);
            _nuviaScript.ChargeLife(1000);
            SessionData.SaveData();
            Debug.Log("Se guardo");
        }
    }
}
