﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Transfoms", menuName = "ScriptableObjects/TransformScriptableObject", order = 2)]

public class TransformPlayerSO : ScriptableObject
{
    public string tipo;
    public GameObject prefab;
    public Texture2D texture;
    public Material material;
    public List<ActionsSO> actions;
    public SkinnedMeshRenderer meshRenderer;
}
