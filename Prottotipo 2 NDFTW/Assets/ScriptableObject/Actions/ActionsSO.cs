﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Actions", menuName = "ScriptableObjects/ActionsScriptableObject", order = 1)]

public class ActionsSO : ScriptableObject
{
    public string name;  // del ataque
    public bool estatic;  //para saber si el personaje se tienee q movere o no
    public bool likewise; // afecta asimismo ej:vida;
    public string mensagge;
    public int argument;
    public bool particle;
    public bool staticParticle;
    public ParticleSystem particleSystem;

    public float timeAction;
    public bool isShader;
    public Shader shader;
    public string animationTrigger;
    
}
